package com.herodav.herodavconsulting.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.herodav.herodavconsulting.data.ChatRoomRepository;
import com.herodav.herodavconsulting.model.ChatRoom;

import java.util.List;

import io.reactivex.Observable;

public class ChatRoomViewModel extends ViewModel {

    ChatRoomRepository mRepository;

    public ChatRoomViewModel() {
        mRepository = new ChatRoomRepository();

    }

    public MutableLiveData<List<ChatRoom>> getChatRooms() {
        return mRepository.getChatRooms();
    }

    public Observable<ChatRoom> getChatRoomById(String chatRoomId){
        return mRepository.getChatRoomById(chatRoomId);
    }
}
