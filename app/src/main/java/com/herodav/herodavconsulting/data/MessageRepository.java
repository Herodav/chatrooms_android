package com.herodav.herodavconsulting.data;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.model.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_CHATROOM_MESSAGES;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_MESSAGE_ID;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_TEXT;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_TIMESTAMP;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_USER_ID;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_CHATROOMS;

public class MessageRepository {

    MutableLiveData<List<Message>> mMessages;

    public MessageRepository() {
        mMessages = new MutableLiveData<>();
    }

    public MutableLiveData<List<Message>> getMessages(String chatRoomId) {

        FirebaseDatabase.getInstance().getReference()
                .child(DB_NODE_CHATROOMS)
                .child(chatRoomId)
                .child(DB_FIELD_CHATROOM_MESSAGES)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {

                            final List<Message> messageList = new ArrayList<>();
                            for (DataSnapshot messageSnapshot : snapshot.getChildren()) {

                                Message message = new Message();
                                HashMap<String, Object> messageMap = (HashMap<String, Object>) messageSnapshot.getValue();

                                if (messageMap != null) {

                                    if (messageMap.get(DB_FIELD_USER_ID) != null) {
                                        message.setUser_id(messageMap.get(DB_FIELD_USER_ID).toString());
                                    }
                                    if (messageMap.get(DB_FIELD_TEXT) != null) {
                                        message.setText(messageMap.get(DB_FIELD_TEXT).toString());
                                    }

                                    if (messageMap.get(DB_FIELD_TIMESTAMP) != null) {
                                        message.setTimestamp(messageMap.get(DB_FIELD_TIMESTAMP).toString());
                                    }

                                    if (messageMap.get(DB_FIELD_MESSAGE_ID) != null) {
                                        message.setMessage_id(messageMap.get(DB_FIELD_MESSAGE_ID).toString());
                                    }
                                    messageList.add(message);
                                }
                            }
                            mMessages.setValue(messageList);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
        return mMessages;
    }
}
