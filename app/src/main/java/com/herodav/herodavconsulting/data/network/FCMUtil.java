package com.herodav.herodavconsulting.data.network;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.herodav.herodavconsulting.model.fcm.Data;
import com.herodav.herodavconsulting.model.fcm.FirebaseCloudMessage;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.herodav.herodavconsulting.utils.Constants.DATA_TYPE_ADMIN_BROADCAST;
import static com.herodav.herodavconsulting.utils.Constants.DATA_TYPE_CHATROOM_MESSAGE;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_SERVER;
import static com.herodav.herodavconsulting.utils.Endpoints.FIREBASE_MESSAGING_URL;

public class FCMUtil {
    private static final String TAG = FCMUtil.class.getSimpleName();
    private String mServerKey;

    private static FCMUtil INSTANCE;

    private FCMUtil() {
    }

    public static FCMUtil getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FCMUtil();
        }
        return INSTANCE;
    }

    public void sendBroadcastMessage(Data data, String token) {
        data.setData_type(DATA_TYPE_ADMIN_BROADCAST);
        sendMessage(data, token);
    }

    public void sendChatMessage(Data data, String token) {
        data.setData_type(DATA_TYPE_CHATROOM_MESSAGE);
       sendMessage(data, token);
    }

    private void sendMessage(Data data, String token) {
        Log.d(TAG, "getServerKey: retrieving server key.");

        final FirebaseCloudMessage message = new FirebaseCloudMessage(token, data);
        FirebaseDatabase.getInstance().getReference().child(DB_NODE_SERVER)
                .orderByValue()
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "onDataChange: got the server key.");
                        DataSnapshot singleSnapshot = dataSnapshot.getChildren().iterator().next();
                        mServerKey = singleSnapshot.getValue().toString();

                        final HashMap<String, String> headers = new HashMap<>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "key=" + mServerKey);

                        ServiceGenerator.createService(FCMService.class, FIREBASE_MESSAGING_URL)
                                .sendMessage(headers, message).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful() && response.body() != null) {
                                    Log.d(TAG, "onResponse: " + new Gson().toJson(response.body()));
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e(TAG, "onFailure: failed to send the message\n Cause: " + t.getMessage());
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


}
