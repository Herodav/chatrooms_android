package com.herodav.herodavconsulting.model;

import com.herodav.herodavconsulting.utils.DateUtil;

import java.util.HashMap;

import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_MESSAGE_ID;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_TEXT;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_TIMESTAMP;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_USER_ID;

public class Message {

    private String timestamp, user_id, text, message_id;

    public Message() {
    }

    public Message(String text, String user_id, String message_id) {
        this.timestamp = DateUtil.getCurrentDate();
        this.text = text;
        this.user_id = user_id;
        this.message_id = message_id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public HashMap<String, Object> toMap() {
        final HashMap<String, Object> map = new HashMap<>();
        map.put(DB_FIELD_TEXT, this.text);
        map.put(DB_FIELD_TIMESTAMP, this.timestamp);
        map.put(DB_FIELD_USER_ID, this.user_id);
        map.put(DB_FIELD_MESSAGE_ID, this.message_id);

        return map;
    }
}
