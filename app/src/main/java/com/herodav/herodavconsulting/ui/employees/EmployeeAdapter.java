package com.herodav.herodavconsulting.ui.employees;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.rpc.ErrorInfoOrBuilder;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.databinding.ItemEmployeeBinding;
import com.herodav.herodavconsulting.model.Employee;
import com.herodav.herodavconsulting.utils.ToastUtil;

import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> {
    private List<Employee> mEmployees;


    @NonNull
    @Override
    public EmployeeAdapter.EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemEmployeeBinding binding = ItemEmployeeBinding.inflate(inflater, parent, false);
        return new EmployeeViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeAdapter.EmployeeViewHolder holder, int position) {

        holder.bind(mEmployees.get(position));
    }

    @Override
    public int getItemCount() {
        return mEmployees.size();
    }

    public void setEmployees(List<Employee> employees) {
        mEmployees = employees;
        notifyDataSetChanged();
    }

    static class EmployeeViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;
        private ItemEmployeeBinding mBinding;

        public EmployeeViewHolder(ItemEmployeeBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            mContext = mBinding.getRoot().getContext();
        }

        public void bind(Employee employee) {
            mBinding.tvName.setText(employee.getFullName());
            if (employee.getProfile_image() != null){
                displayProfileImage(Uri.parse(employee.getProfile_image()));
            }

            if (employee.getDepartmentId() == null) {
                mBinding.btnDelete.setVisibility(View.INVISIBLE);
            } else {
                mBinding.btnDelete.setOnClickListener(bnt -> deleteFromDepartment(employee));
            }
        }

        private void deleteFromDepartment(Employee employee) {
            ToastUtil.showCentredToast(mContext, "To be implemented");
        }

        private void displayProfileImage(Uri uri) {
            Glide.with(mContext)
                    .load(uri)
                    .circleCrop()
                    .placeholder(R.drawable.profile_placeholder)
                    .into( mBinding.imgProfile);
        }
    }


}
