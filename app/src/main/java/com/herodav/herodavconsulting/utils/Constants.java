package com.herodav.herodavconsulting.utils;

public class Constants {

    //Firebase DB
    public static final String DB_NODE_USERS = "users";
    public static final String DB_FIELD_NAME = "name";
    public static final String DB_FIELD_SURNAME = "surname";
    public static final String DB_FIELD_PHONE = "phone";
    public static final String DB_FIELD_PROFILE_IMAGE = "profile_image";
    public static final String DB_FIELD_USER_ID = "user_id";
    public static final String DB_FIELD_IS_ADMIN = "is_admin";
    public static final String DB_FIELD_MESSAGING_TOKEN = "messaging_token";

    public static final String DB_NODE_CHATROOMS = "chatrooms";
    public static final String DB_FIELD_CHATROOM_ID = "chatroom_id";
    public static final String DB_FIELD_CHATROOM_TITLE = "chatroom_title";
    public static final String DB_FIELD_CHATROOM_MESSAGES = "chatroom_messages";
    public static final String DB_FIELD_CREATOR_ID = "creator_id";
    public static final String DB_FIELD_TEXT = "text";
    public static final String DB_FIELD_TIMESTAMP = "timestamp";
    public static final String DB_FIELD_MESSAGE_ID = "message_id";

    public static final String DB_NODE_DEPARTMENTS = "departments";
    public static final String DB_FIELD_DEPARTMENT_ID = "department_id";
    public static final String DB_FIELD_DEPARTMENT_NAME = "department_name";
    public static final String DB_FIELD_DEPARTMENT_EMPLOYEES = "department_employees";
    public static final String DB_FIELD_EMPLOYEES_BRANCH_KEY = "employees_branch_key";

    public static final String DB_NODE_SERVER = "server";
    public static final String DB_FIELD_SERVER_KEY = "server_key";


    //fireStore
    public static final String DEFAULT_PROFILE_IMAGE_NAME = "profile_image";
    public static final String STORAGE_IMAGES = "images";
    public static final String STORAGE_FIELD_USERS = "images/users/";
    public static final String STORAGE_FIELD_PROFILE_IMAGE = "/profile_image.jpg";

    //firebase file size
    public static final double MB_THRESHOLD = 5.0;
    public static final double MB = 1000000.0;

    //Push notification
    public static final String MESSAGE_DATA_TYPE = "data_type";
    public static final String DATA_TYPE_ADMIN_BROADCAST = "data_type_admin_broadcast";
    public static final String DATA_TYPE_CHATROOM_MESSAGE = "data_type_chatroom_message";
    public static final String DATA_TITLE =  "title";
    public static final String DATA_USER_NAME = "user_name";
    public static final String DATA_MESSAGE = "message";
    public static final String DATA_CHAT_ID = "chat_id";

    //camera authority
    public static final String FILE_PROVIDER_AUTHORITY = "com.herodav.herodavconsulting.fileprovider";


    public static final String ACTION_OPEN_CHAT = "ACTION_OPEN_CHAT";
}
