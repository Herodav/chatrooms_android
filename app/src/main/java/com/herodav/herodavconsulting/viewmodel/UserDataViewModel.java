package com.herodav.herodavconsulting.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.model.User;

import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_USERS;

public class UserDataViewModel extends ViewModel {

    MutableLiveData<User> mUser;

    public UserDataViewModel() {
        mUser = new MutableLiveData<>();

        FirebaseUser fbUser = FirebaseAuth.getInstance().getCurrentUser();
        if (fbUser != null) {

            ValueEventListener listener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.hasChildren()) {
                        mUser.setValue(snapshot.getValue(User.class));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            };

            DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference();
            dbReference.child(DB_NODE_USERS)
                    .child(fbUser.getUid()).addValueEventListener(listener);

        }
    }

    public MutableLiveData<User> getUser() {
        return mUser;
    }

}
