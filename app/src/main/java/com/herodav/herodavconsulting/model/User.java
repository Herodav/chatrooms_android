package com.herodav.herodavconsulting.model;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class User {

    private String name;
    private String surname;
    private String phone;
    private String profile_image;
    private String security_level;
    private String user_id;
    private boolean is_admin;
    private String department_id;
    private String messaging_token;


    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getSecurity_level() {
        return security_level;
    }

    public void setSecurity_level(String security_level) {
        this.security_level = security_level;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public boolean isIs_admin() {
        return is_admin;
    }

    public void setIs_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getMessaging_token() {
        return messaging_token;
    }

    public void setMessaging_token(String messaging_token) {
        this.messaging_token = messaging_token;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("surname", surname);
        result.put("phone", phone);
        result.put("profile_image", profile_image);
        result.put("security_level", security_level);
        result.put("user_id", user_id);
        result.put("is_admin", is_admin);
        result.put("department_id", department_id);
        result.put("messaging_token", messaging_token);
        return result;
    }

    public String getFullName() { //todo: test it cause not working in employee adapter
        final String name = this.getName() != null ? this.getName() : "";
        final String surname = this.getSurname() != null ? this.getSurname() : "";
        final String fullName = String.format("%s %s", name, surname);
        return fullName.isEmpty() ? "unknown" : fullName;
    }

    @Override
    @NonNull
    public String toString() {
        return new Gson().toJson(this);
    }
}
