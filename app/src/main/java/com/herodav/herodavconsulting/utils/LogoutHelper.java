package com.herodav.herodavconsulting.utils;

import com.google.firebase.auth.FirebaseAuth;

public class LogoutHelper {
    public static void logout() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            auth.signOut();
        }
    }
}
