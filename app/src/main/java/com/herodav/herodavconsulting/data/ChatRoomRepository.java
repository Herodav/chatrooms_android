package com.herodav.herodavconsulting.data;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.model.ChatRoom;
import com.herodav.herodavconsulting.model.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_CHATROOM_ID;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_CHATROOM_MESSAGES;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_CHATROOM_TITLE;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_CREATOR_ID;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_TEXT;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_TIMESTAMP;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_USER_ID;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_CHATROOMS;

public class ChatRoomRepository {

    private MutableLiveData<List<ChatRoom>> mChatRooms;

    public ChatRoomRepository() {
        mChatRooms = new MutableLiveData<>();
    }

    public Observable<ChatRoom> getChatRoomById(String chatRoomId) {
        final PublishSubject<ChatRoom> chatRoomObs = PublishSubject.create();

        FirebaseDatabase.getInstance().getReference().child(DB_NODE_CHATROOMS)
                .child(chatRoomId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {

                            final ChatRoom chatRoom = new ChatRoom();
                            final HashMap<String, Object> chatMap
                                    = (HashMap<String, Object>) snapshot.getValue();

                            if (chatMap != null) {
                                //set fields only if the are not null
                                if (chatMap.get(DB_FIELD_CHATROOM_ID) != null) {
                                    chatRoom.setChatroom_id(chatMap.get(DB_FIELD_CHATROOM_ID).toString());
                                }

                                if (chatMap.get(DB_FIELD_CHATROOM_TITLE) != null) {
                                    chatRoom.setChatroom_title(chatMap.get(DB_FIELD_CHATROOM_TITLE).toString());
                                }

                                if (chatMap.get(DB_FIELD_CREATOR_ID) != null) {
                                    chatRoom.setCreator_id(chatMap.get(DB_FIELD_CREATOR_ID).toString());
                                }
                                chatRoom.setMessages(getMessages(snapshot));

                                chatRoomObs.onNext(chatRoom);
                                chatRoomObs.onComplete();
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
        return chatRoomObs;
    }

    public MutableLiveData<List<ChatRoom>> getChatRooms() {

        FirebaseDatabase.getInstance().getReference().child(DB_NODE_CHATROOMS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            final List<ChatRoom> chatRoomList = new ArrayList<>();

                            for (DataSnapshot chatRoomSnapshot : snapshot.getChildren()) {

                                final ChatRoom chatRoom = new ChatRoom();
                                final HashMap<String, Object> chatMap
                                        = (HashMap<String, Object>) chatRoomSnapshot.getValue();

                                if (chatMap != null) {
                                    //set fields only if the are not null
                                    if (chatMap.get(DB_FIELD_CHATROOM_ID) != null) {
                                        chatRoom.setChatroom_id(chatMap.get(DB_FIELD_CHATROOM_ID).toString());
                                    }

                                    if (chatMap.get(DB_FIELD_CHATROOM_TITLE) != null) {
                                        chatRoom.setChatroom_title(chatMap.get(DB_FIELD_CHATROOM_TITLE).toString());
                                    }

                                    if (chatMap.get(DB_FIELD_CREATOR_ID) != null) {
                                        chatRoom.setCreator_id(chatMap.get(DB_FIELD_CREATOR_ID).toString());
                                    }
                                    //todo: get last message
                                    chatRoom.setMessages(getMessages(chatRoomSnapshot));
                                    chatRoomList.add(chatRoom);
                                }
                            }
                            mChatRooms.setValue(chatRoomList);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
        return mChatRooms;
    }

    //todo: use this to ge last message details only, not all messages
    private List<Message> getMessages(DataSnapshot chatRoomSnapshot) {

        final List<Message> messageList = new ArrayList<>();

        for (DataSnapshot messageSnapshot :
                chatRoomSnapshot.child(DB_FIELD_CHATROOM_MESSAGES).getChildren()) {
            if (messageSnapshot.exists()) {

                final Message message = new Message();
                final HashMap<String, Object> messageMap = (HashMap<String, Object>) messageSnapshot.getValue();

                if (messageMap != null) {

                    if (messageMap.get(DB_FIELD_TIMESTAMP) != null) {
                        message.setTimestamp(messageMap.get(DB_FIELD_TIMESTAMP).toString());
                    }
                    if (messageMap.get(DB_FIELD_TEXT) != null) {
                        message.setText(messageMap.get(DB_FIELD_TEXT).toString());
                    }
                    if (messageMap.get(DB_FIELD_USER_ID) != null) {
                        message.setUser_id(messageMap.get(DB_FIELD_USER_ID).toString());
                    }
                    messageList.add(message);
                }

            }
        }

        return messageList;
    }
}
