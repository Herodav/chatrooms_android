package com.herodav.herodavconsulting.data;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.model.Department;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_DEPARTMENT_ID;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_DEPARTMENT_NAME;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_EMPLOYEES_BRANCH_KEY;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_DEPARTMENTS;

public class DepartmentRepository {
    private static final String TAG = DepartmentRepository.class.getSimpleName();

    private MutableLiveData<List<Department>> mDepartments;

    private PublishSubject<Set<String>> mUserIdsObs;

    public DepartmentRepository() {
        mDepartments = new MutableLiveData<>();
        mUserIdsObs = PublishSubject.create();
    }

    public MutableLiveData<List<Department>> getDepartments() {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child(DB_NODE_DEPARTMENTS);

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    final List<Department> departments = new ArrayList<>();

                    for (DataSnapshot departmentSnapshot : snapshot.getChildren()) {

                        final Department department = new Department();
                        final HashMap<String, Object> departmentMap
                                = (HashMap<String, Object>) departmentSnapshot.getValue();

                        if (departmentMap != null) {
                            //add the fields if the exist

                            if (departmentMap.get(DB_FIELD_DEPARTMENT_ID) != null) {
                                department.setDepartment_id(
                                        departmentMap.get(DB_FIELD_DEPARTMENT_ID).toString());
                            }

                            if (departmentMap.get(DB_FIELD_DEPARTMENT_NAME) != null) {
                                department.setDepartment_name(
                                        departmentMap.get(DB_FIELD_DEPARTMENT_NAME).toString());
                            }

                            //add the idSet
                            if (departmentMap.get(DB_FIELD_EMPLOYEES_BRANCH_KEY) != null) {
                                final String employeesBranchKey = departmentMap.get(DB_FIELD_EMPLOYEES_BRANCH_KEY).toString();
                                department.setEmployees_branch_key(employeesBranchKey);

                                if (departmentMap.get(employeesBranchKey) != null) {
                                    final HashMap<String, Object> idsMap
                                            = (HashMap<String, Object>) departmentMap.get(employeesBranchKey);
                                    final Set<String> idSet = new HashSet<>(idsMap.keySet());
                                    if (idsMap != null && !idsMap.isEmpty()) {
                                        department.setDepartment_employeeIds((HashSet<String>) idSet);
                                    }
                                }
                            }
                            departments.add(department);
                        }
                    }
                    mDepartments.setValue(departments);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        return mDepartments;
    }
}
