package com.herodav.herodavconsulting.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.firebase.auth.FirebaseUser;
import com.herodav.herodavconsulting.MainActivity;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.model.fcm.Data;
import com.herodav.herodavconsulting.utils.Constants;

import static android.app.NotificationManager.IMPORTANCE_HIGH;
import static android.content.Context.NOTIFICATION_SERVICE;
import static androidx.core.app.NotificationCompat.DEFAULT_ALL;
import static androidx.core.app.NotificationCompat.PRIORITY_HIGH;
import static com.herodav.herodavconsulting.utils.Constants.DATA_CHAT_ID;
import static com.herodav.herodavconsulting.utils.Constants.DATA_TITLE;

public class ChatNotification extends AppNotification {

    public static final String CHAT_NOTIFICATION_CHANNEL_ID = "chat_notification_channel";
    public static final String CHAT_NOTIFICATION_NAME = "Herodav Consulting";
    public static final int NOTIFICATION_ID = 2;

    private Uri mNotificationSound;
    private Context mContext;
    private Data mData;


    public ChatNotification(Context context, Data data) {
        this.mContext = context;
        this.mData = data;
        mNotificationSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + context.getPackageName() + "/" + R.raw.insight);
    }

    public void sendNotification() {
        NotificationManager notificationManager = (NotificationManager)
                mContext.getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            createChannel(notificationManager);
            NotificationCompat.Builder notificationBuilder
                    = new NotificationCompat.Builder(mContext, CHAT_NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(mData.getTitle() + "(~" + mData.getUser_name() + "~)")
                    .setContentText(mData.getMessage())
                    .setSound(mNotificationSound)
                    .setSmallIcon(R.drawable.ic_department_48)
                    .setLargeIcon(BitmapFactory.decodeResource(
                            mContext.getResources()
                            , R.drawable.ic_department_48
                    ))
                    .setContentIntent(createMainActivityIntent(mContext))
                    .setPriority(PRIORITY_HIGH)
                    .setDefaults(DEFAULT_ALL)
                    .setAutoCancel(true);

            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    @Override
    public void sendNotification(String title, String text) {
        NotificationManager notificationManager = (NotificationManager)
                mContext.getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            createChannel(notificationManager);
            NotificationCompat.Builder notificationBuilder
                    = new NotificationCompat.Builder(mContext, CHAT_NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setSound(mNotificationSound)
                    .setSmallIcon(R.drawable.ic_department_48)
                    .setLargeIcon(BitmapFactory.decodeResource(
                            mContext.getResources()
                            , R.drawable.ic_department_48
                    ))
                    .setContentIntent(createMainActivityIntent(mContext))
                    .setPriority(PRIORITY_HIGH)
                    .setDefaults(DEFAULT_ALL)
                    .setAutoCancel(true);

            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    @Override
    void createChannel(NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHAT_NOTIFICATION_CHANNEL_ID,
                    CHAT_NOTIFICATION_NAME, IMPORTANCE_HIGH);
            channel.enableLights(true);
            channel.setLightColor(Color.YELLOW);
            channel.enableVibration(true);
            channel.setSound(mNotificationSound,
                    new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .build());
            channel.setDescription("Broadcast from Herodav Consulting");

            notificationManager.createNotificationChannel(channel);
        }
    }

    private PendingIntent createMainActivityIntent(Context context) {
        Intent startMainActivityIntent = new Intent(context, MainActivity.class);
        startMainActivityIntent.setAction(Constants.ACTION_OPEN_CHAT);
        startMainActivityIntent.putExtra(DATA_CHAT_ID, mData.getChat_id());
        startMainActivityIntent.putExtra(DATA_TITLE, mData.getTitle());
        startMainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return PendingIntent.getActivity(context,
                NOTIFICATION_ID,
                startMainActivityIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
