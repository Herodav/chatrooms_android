package com.herodav.herodavconsulting.ui.editProfile;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.databinding.FragmentEditProfileBinding;
import com.herodav.herodavconsulting.model.User;
import com.herodav.herodavconsulting.utils.ImageUtil;
import com.herodav.herodavconsulting.utils.InputValidatorHelper;
import com.herodav.herodavconsulting.utils.LogoutHelper;
import com.herodav.herodavconsulting.utils.SnackBarUtil;
import com.herodav.herodavconsulting.utils.ToastUtil;
import com.herodav.herodavconsulting.viewmodel.UserDataViewModel;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_PROFILE_IMAGE;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_USERS;
import static com.herodav.herodavconsulting.utils.Constants.DEFAULT_PROFILE_IMAGE_NAME;
import static com.herodav.herodavconsulting.utils.Constants.FILE_PROVIDER_AUTHORITY;
import static com.herodav.herodavconsulting.utils.Constants.MB;
import static com.herodav.herodavconsulting.utils.Constants.MB_THRESHOLD;
import static com.herodav.herodavconsulting.utils.Constants.STORAGE_FIELD_PROFILE_IMAGE;
import static com.herodav.herodavconsulting.utils.Constants.STORAGE_FIELD_USERS;

public class EditProfileFragment extends Fragment {
    private static final String TAG = EditProfileFragment.class.getSimpleName();
    private static final int PICK_IMAGE_REQUEST_CODE = 1;
    private static final int CAPTURE_IMAGE_REQUEST_CODE = 2;
    private static final int ALL_PERMISSIONS_REQUEST_CODE = 3;

    private FragmentEditProfileBinding mBinding;
    private ProgressDialog mProgressDialog;

    FirebaseAuth mAuth;

    private UserDataViewModel mUserDataViewModel;

    private Uri mImageUri;
    private File mImageFile;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        mUserDataViewModel = new ViewModelProvider(this).get(UserDataViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentEditProfileBinding.inflate(inflater, container, false);
        setupUi();
        updateUi();
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!permissionsGranted()) {
            requestPermissions();
        }
    }

    private void updateUi() {
        showProgress();
        mBinding.edtPassword.setText("");

        mUserDataViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                final String name = user.getName();
                final String surname = user.getSurname();
                final String phone = user.getPhone();
                final String profileUrl = user.getProfile_image();

                if (name != null) {
                    mBinding.edtName.setText(name);
                }
                if (surname != null) {
                    mBinding.edtSurname.setText(surname);
                }
                if (phone != null) {
                    mBinding.edtPhone.setText(phone);
                }
                if (profileUrl != null) {
                    displayProfileImage(Uri.parse(profileUrl));
                }
            }
            hideProgress();
        });
    }

    private void setupUi() {
        mBinding.btnConfirm.setOnClickListener(v1 -> {
            if (isValidForm()) {
                attemptProfileUpdate();
            }
        });
        mBinding.tvResetPwd.setOnClickListener(v1 -> sendPasswordResetEmail());
        mBinding.imgSelect.setOnClickListener(v1 -> displaySelectImageDialog());
    }

    private void displaySelectImageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(R.string.select_image)
                .setItems(R.array.img_select_options, (dialog, item) -> {
                    switch (item) {
                        case 0:
                            captureImage();
                            break;
                        case 1:
                            selectImageFromGallery();
                    }
                });
        builder.show();
    }

    private void selectImageFromGallery() {
        if (!permissionsGranted()) {
            requestPermissions();
            return;
        }
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK);
        pickImageIntent.setType("image/*");
        startActivityForResult(pickImageIntent, PICK_IMAGE_REQUEST_CODE);
    }

    private boolean permissionsGranted() {
        int readStoragePermissionState = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermissionState = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.CAMERA);

        return readStoragePermissionState == PackageManager.PERMISSION_GRANTED
                && cameraPermissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void attemptProfileUpdate() {
        showProgress();

        final String password = mBinding.edtPassword.getText().toString();

        //re-authenticate to prevent unauthorised update
        FirebaseUser fbUser = mAuth.getCurrentUser();
        AuthCredential credential = EmailAuthProvider.getCredential(fbUser.getEmail(), password);

        fbUser.reauthenticate(credential).addOnCompleteListener(reauthTask -> {
            if (reauthTask.isSuccessful()) {
                updateProfile();
            }
        }).addOnFailureListener(e -> {
            hideProgress();
            ToastUtil.showCentredToast(getContext(), e.getMessage());
        });

    }

    private void sendPasswordResetEmail() {
        showProgress();

        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null && user.getEmail() != null) {
            mAuth.sendPasswordResetEmail(user.getEmail()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    SnackBarUtil.showSnackbar(getActivity(), R.string.prompt_reset_password, android.R.string.ok
                            , v -> LogoutHelper.logout());
                }
                hideProgress();
            })
                    .addOnFailureListener(e -> {
                        hideProgress();
                        ToastUtil.showCentredToast(getContext(), e.getMessage());
                        Log.e(TAG, "resetPassword: ", e);
                        LogoutHelper.logout();
                    });
        }
    }

    private void updateProfile() {
        showProgress();

        final String name = mBinding.edtName.getText().toString().trim();
        final String surname = mBinding.edtSurname.getText().toString().trim();
        final String phoneNumber = mBinding.edtPhone.getText().toString().trim();

        FirebaseUser fbUser = mAuth.getCurrentUser();
        DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference();

        final User user = new User();
        user.setName(name);
        user.setPhone(phoneNumber);
        user.setSurname(surname);

        dbReference.child(DB_NODE_USERS).child(fbUser.getUid())
                .updateChildren(user.toMap())
                .addOnCompleteListener(updateTask -> {
                    if (updateTask.isSuccessful()) {
                        if (mImageUri != null) {
                            upLoadImage(mImageUri);
                        } else {
                            hideProgress();
                            updateUi();
                            ToastUtil.showCentredToast(getContext(), getString(R.string.update_success));
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    hideProgress();
                    ToastUtil.showCentredToast(getContext(), e.getMessage());
                });
    }

    private void upLoadImage(Uri uri) {
        showProgress();
        final FirebaseUser fbUser = mAuth.getCurrentUser();

        try {

            final File compressedImage = ImageUtil.getCompressedImage(getContext(), uri);
            if (compressedImage != null) {
                if ((compressedImage.length() / MB) < MB_THRESHOLD) {//file not bigger than required size

                    Log.e(TAG, String.format("uploadPicture: Photo size is %sKB",
                            Math.round(compressedImage.length() / (1024.0))));

                    final StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                            .child(STORAGE_FIELD_USERS + fbUser.getUid()
                                    + STORAGE_FIELD_PROFILE_IMAGE); //JUST REPLACE IMAGE

                    storageReference.putFile(Uri.fromFile(compressedImage))
                            .addOnProgressListener(snapshot -> {
/*
                                double currentProgress = (100.0 * snapshot.getBytesTransferred()) / snapshot.getTotalByteCount();
                                if (currentProgress % 20 == 0) {
                                    double progress = (100.0 * snapshot.getBytesTransferred()) / snapshot.getTotalByteCount();
                                    Log.d(TAG, "onProgress: Upload is " + progress + "% done");
                                    Toast.makeText(getContext(), progress + "%", Toast.LENGTH_SHORT).show();
                                }*/
                            })
                            .continueWithTask(task -> {
                                if (!task.isSuccessful()) {
                                    throw task.getException();
                                }
                                // Continue with the task to get the download URL
                                return storageReference.getDownloadUrl();
                            })
                            .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {
                                    Uri downloadUri = task.getResult();
                                    DatabaseReference dbRef =
                                            FirebaseDatabase.getInstance().getReference()
                                                    .child(DB_NODE_USERS).child(fbUser.getUid())
                                                    .child(DB_FIELD_PROFILE_IMAGE);

                                    dbRef.setValue(downloadUri.toString())
                                            .addOnFailureListener(e -> Log.e(TAG, "upLoadImage: "
                                                    + e.getMessage()));

                                    hideProgress();
                                    updateUi();
                                }
                            })
                            .addOnFailureListener(e -> {
                                hideProgress();
                                ToastUtil.showCentredToast(getContext(), e.getMessage());
                            });

                } else {
                    hideProgress();
                    ToastUtil.showCentredToast(getContext(), getString(R.string.error_photo_size));

                    Log.e(TAG, String.format("uploadPicture: Photo size is %sKB",
                            Math.round(compressedImage.length() / (1024.0))));
                }
            }

        } catch (IOException e) {
            hideProgress();
            ToastUtil.showCentredToast(getContext(), getString(R.string.error_image_upload));
            e.printStackTrace();
            Log.e(TAG, "upLoadImage: Error compressing the image");
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    mImageUri = data.getData();

                    try {
                        cropImage(mImageUri);
                    } catch (Exception e) {
                        Log.e(TAG, "onActivityResult: error cropping th image from gallery");
                        displayProfileImage(mImageUri);
                    }

                } else {
                    ToastUtil.showCentredToast(getContext(), R.string.gallery_error);
                    Log.e(TAG, "onActivityResult: failed to load image from Gallery");
                }
                break;
            case CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {

                    mImageUri = FileProvider.getUriForFile(getActivity(),
                            FILE_PROVIDER_AUTHORITY,
                            mImageFile);
                    try {
                        cropImage(mImageUri);
                    } catch (Exception e) {
                        Log.e(TAG, "onActivityResult: Error cropping captured image", e);
                        displayProfileImage(mImageUri);
                    }
                }
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK && data != null) {

                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    mImageUri = result.getUri();
                    displayProfileImage(mImageUri);

                } else {
                    displayProfileImage(mImageUri);
                    ToastUtil.showCentredToast(getContext(), R.string.error_cropping_image);
                    Log.e(TAG, "onActivityResult: failed to crop Image");
                }
                break;
            default:
                break;
        }
    }

    private void cropImage(Uri uri) {
        if (uri == null) {
            return;
        }
        CropImage.activity(uri).setAspectRatio(1, 1)
                .start(getContext(), this);
          /*  Intent cropIntent = new Intent(CROP_APP_PACKAGE);

            cropIntent.setDataAndType(uri, "image/*");

            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);

            cropIntent.putExtra("outputX", 200);
            cropIntent.putExtra("outputY", 200);

            cropIntent.putExtra("return-data", true);

            startActivityForResult(cropIntent, CROP_IMAGE_REQUEST_CODE);*/
    }

   /*
    private void updateEmail() {
        showProgress();

        final String name = edtName.getText().toString().trim();
        final String surname = edtSurname.getText().toString().trim();
        final String email = edtEmail.getText().toString().trim();
        final String phoneNumber = edtPhone.getText().toString().trim();
        final String password = edtPassword.getText().toString();

        //re-authenticate to prevent unauthorised pwd reset
        FirebaseUser user = mAuth.getCurrentUser();
        AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), password);

        user.reauthenticate(credential).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                //ensure the email is not yet in use
                mAuth.fetchSignInMethodsForEmail(email).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {

                        if (task1.getResult().getSignInMethods().size() > 0) {//email exists
                            ToastUtil.showCentredToast(getContext(), "Sorry, email already in use");
                        } else {
                            user.updateEmail(email)
                                    .addOnCompleteListener(task2 -> {
                                        if (task2.isSuccessful()) {
                                            LogoutHelper.logout();
                                            ToastUtil.showCentredToast(getContext(), "Updated email");
                                        }
                                    }).addOnFailureListener(e -> {
                                Log.e(TAG, "updateProfileAndEmail: ", e);
                            });
                        }
                    }
                    hideProgress();
                }).addOnFailureListener(e -> {
                    hideProgress();
                    ToastUtil.showCentredToast(getContext(), e.getMessage());
                });

            }
            hideProgress();
        }).addOnFailureListener(e -> {
            hideProgress();
            ToastUtil.showCentredToast(getContext(), e.getMessage());
        });


    }*/

    private void captureImage() {
        //create IMG file first
        try {
            createImageFile();
        } catch (IOException e) {
            Toast.makeText(getActivity(), R.string.camera_error, Toast.LENGTH_LONG).show();
            Log.e(TAG, "captureImage: unable to create file : ", e);
            return;
        }

        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        boolean canTakePhoto =
                captureImage.resolveActivity(getActivity().getPackageManager()) != null;
        if (!canTakePhoto) {
            Toast.makeText(getActivity(), R.string.camera_error, Toast.LENGTH_LONG).show();
            return;
        }
        if (!permissionsGranted()) {
            requestPermissions();
            return;
        }

        final Uri uri = FileProvider.getUriForFile(getActivity(),
                FILE_PROVIDER_AUTHORITY,
                mImageFile);
        captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        List<ResolveInfo> cameraActivities = getActivity()
                .getPackageManager().queryIntentActivities(captureImage,
                        PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo activity : cameraActivities) {
            getActivity().grantUriPermission(activity.activityInfo.packageName,
                    uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        startActivityForResult(captureImage, CAPTURE_IMAGE_REQUEST_CODE);
    }

    private File createImageFile() throws IOException {
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        mImageFile = File.createTempFile(DEFAULT_PROFILE_IMAGE_NAME, ".jpg", storageDir);
        return mImageFile;
    }

    private void displayProfileImage(Uri uri) {
        Glide.with(this)
                .load(uri)
                .circleCrop()
                .placeholder(R.drawable.profile_placeholder)
                .into(mBinding.imgProfile);
    }

    private void displayProfileImage(Bitmap image) {
        Glide.with(this)
                .load(image)
                .circleCrop()
                .placeholder(R.drawable.profile_placeholder)
                .into(mBinding.imgProfile);
    }

    private void requestPermissions() {
        String[] permissions = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}; // add other permissions to this list

        boolean shouldProvideRationale =
                shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) ||
                        shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            SnackBarUtil.showSnackbar(getActivity(), R.string.permission_all,
                    android.R.string.ok, view -> {
                        // Request permission
                        requestPermissions(permissions, ALL_PERMISSIONS_REQUEST_CODE);
                    });
        } else {
            requestPermissions(permissions, ALL_PERMISSIONS_REQUEST_CODE);
        }
    }

    private boolean isValidForm() {
        InputValidatorHelper inputValidatorHelper = new InputValidatorHelper();

        boolean isPhoneValid = inputValidatorHelper
                .isCellNumber(mBinding.edtPhone.getText().toString());
        if (!isPhoneValid) {
            mBinding.edtPhone.requestFocus();
            mBinding.edtPhone.setError(getString(R.string.invalid_phone_Number));
        }

        return (inputValidatorHelper
                .isValidForm(getString(R.string.error_field_required),
                        mBinding.edtName, mBinding.edtSurname, mBinding.edtPhone, mBinding.edtPassword));

    }

    private void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void showProgress() {
        if (mProgressDialog == null || !mProgressDialog.isShowing()) {
            mProgressDialog = ProgressDialog.show(getContext(), null, getString(R.string.please_wait));
        }
    }

}