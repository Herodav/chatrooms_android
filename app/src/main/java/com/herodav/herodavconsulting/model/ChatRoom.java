package com.herodav.herodavconsulting.model;

import java.util.List;

public class ChatRoom {
    private String chatroom_id, chatroom_title, creator_id;
    private List<Message> mMessages;

    public ChatRoom() {
    }

    public String getChatroom_id() {
        return chatroom_id;
    }

    public void setChatroom_id(String chatroom_id) {
        this.chatroom_id = chatroom_id;
    }

    public String getChatroom_title() {
        return chatroom_title;
    }

    public void setChatroom_title(String chatroom_title) {
        this.chatroom_title = chatroom_title;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public List<Message> getMessages() {
        return mMessages;
    }

    public void setMessages(List<Message> messages) {
        mMessages = messages;
    }
}
