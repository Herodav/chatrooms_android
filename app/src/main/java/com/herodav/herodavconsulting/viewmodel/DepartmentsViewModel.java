package com.herodav.herodavconsulting.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.herodav.herodavconsulting.data.DepartmentRepository;
import com.herodav.herodavconsulting.model.Department;

import java.util.List;
import java.util.Set;

import io.reactivex.Observable;

public class DepartmentsViewModel extends ViewModel {

    DepartmentRepository mDepartmentRepository;

    public DepartmentsViewModel() {
        mDepartmentRepository = new DepartmentRepository();
    }

    public LiveData<List<Department>> getDepartments() {
        return mDepartmentRepository.getDepartments();
    }
}
