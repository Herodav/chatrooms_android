package com.herodav.herodavconsulting.ui.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.databinding.FragmentLoginBinding;
import com.herodav.herodavconsulting.ui.FragmentNoActionBar;
import com.herodav.herodavconsulting.utils.SnackBarUtil;
import com.herodav.herodavconsulting.utils.ToastUtil;

public class LoginFragment extends FragmentNoActionBar {
    private static final String TAG = LoginFragment.class.getSimpleName();

    private FragmentLoginBinding mBinding;
    private ProgressDialog mProgressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentLoginBinding.inflate(inflater, container, false);
        verifyUserLoginSate();
        setupUi();
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    private void setupUi() {
        mBinding.btnToggle.setOnClickListener(btn -> navigateToSignUp());
        mBinding.tvResendVerification.setOnClickListener(tv -> showResendDialog());
        mBinding.btnLogin.setOnClickListener(btn -> {
            if (isValidFrom()) {
                attemptLogin();
            }
        });
    }

    private void verifyUserLoginSate() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if (user.isEmailVerified()) {
                navigateToDashBoard();
            }
        }
    }

    private void showResendDialog() {
        new ResendVerificationDialog().show(getActivity().getSupportFragmentManager(), "resend_dialog");
    }

    private void attemptLogin() {
        showProgress();
        final String email = mBinding.edtEmail.getText().toString();
        final String password = mBinding.edtPassword.getText().toString();
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        if (user != null) {
                            if (user.isEmailVerified()) {
                                //navigation to Dashboard handled in MainActivity
                                ToastUtil.showCentredToast(getContext(), getString(R.string.welcome_message));
                            } else {
                                FirebaseAuth.getInstance().signOut();
                                SnackBarUtil.showSnackbar(getActivity(), R.string.prompt_check_verification_link,
                                        android.R.string.ok, v -> {
                                            //dismiss
                                        });
                            }
                        }
                    }
                    hideProgress();
                }).addOnFailureListener(e -> {
            ToastUtil.showCentredToast(getContext(), e.getMessage());
        });
    }

    private void navigateToSignUp() {
        Navigation.findNavController(getActivity(), R.id.nav_host_fragment)
                .navigate(R.id.signUpFragment);
    }

    private void navigateToDashBoard() {
        Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(
                R.id.chatRoomListFragment,
                null,
                new NavOptions.Builder()
                        .setPopUpTo(R.id.chatRoomListFragment, true)
                        .setPopEnterAnim(android.R.anim.slide_in_left)
                        .build());
    }

    private boolean isValidFrom() {
        //todo: implement
        boolean isValid = true;

        if (mBinding.edtEmail.getText().toString().isEmpty()) {
            mBinding.edtEmail.setError(getString(R.string.error_field_required));
            isValid = false;
        }
        if (mBinding.edtPassword.getText().toString().isEmpty()) {
            mBinding.edtPassword.setError(getString(R.string.error_field_required));
            isValid = false;
        }
        return isValid;
    }

    private void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void showProgress() {
        if (mProgressDialog == null || !mProgressDialog.isShowing()) {
            mProgressDialog = ProgressDialog.show(getContext(), null, "Please wait..");
        }
    }
}