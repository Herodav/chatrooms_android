package com.herodav.herodavconsulting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.herodav.herodavconsulting.model.ChatRoom;
import com.herodav.herodavconsulting.model.Department;
import com.herodav.herodavconsulting.ui.ActionBarHandler;
import com.herodav.herodavconsulting.ui.chat.ChatFragment;
import com.herodav.herodavconsulting.ui.chatRoomList.ChatRoomAdapter;
import com.herodav.herodavconsulting.ui.departmentDetail.DepartmentDetailFragment;
import com.herodav.herodavconsulting.ui.departmentList.DepartmentListFragment;
import com.herodav.herodavconsulting.utils.LogoutHelper;
import com.herodav.herodavconsulting.utils.SnackBarUtil;

import static com.herodav.herodavconsulting.utils.Constants.ACTION_OPEN_CHAT;
import static com.herodav.herodavconsulting.utils.Constants.DATA_CHAT_ID;
import static com.herodav.herodavconsulting.utils.Constants.DATA_TITLE;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_MESSAGING_TOKEN;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_USERS;

public class MainActivity extends AppCompatActivity implements ActionBarHandler,
        ChatRoomAdapter.ChatRoomHolder.HolderCallBack,
        DepartmentListFragment.DepartmentHolder.HolderCallback {
    private static final String TAG = MainActivity.class.getSimpleName();

    private AppBarConfiguration mAppBarConfiguration;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private NavController mNavController;
    private FirebaseUser mCurrentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Firebase stuff
        setupFirebaseAuthListener();
        initFCM();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.loginFragment,
                R.id.chatRoomListFragment)
                .build();
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        mNavController = navHostFragment.getNavController();
        NavigationUI.setupActionBarWithNavController(this, mNavController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(toolbar, mNavController, mAppBarConfiguration);

        if (getIntent() != null) {
            attemptNavigateToChatFragment(getIntent());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupFirebaseAuthListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthStateListener != null) {
            FirebaseAuth.getInstance().removeAuthStateListener(mAuthStateListener);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(mAuthStateListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                LogoutHelper.logout();
                return true;
            case R.id.action_edit_profile:
                navigateToEditProfile();
                return true;
            case R.id.action_admin:
//                todo: disable if the user is not an admin
                navigateToAdmin();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        attemptNavigateToChatFragment(intent);
    }

    private void attemptNavigateToChatFragment(Intent intent) {
        if (intent.getAction() != null && intent.getAction().equals(ACTION_OPEN_CHAT)) {
            final String chatTitle = intent.getStringExtra(DATA_TITLE);
            final String chatId = intent.getStringExtra(DATA_CHAT_ID);
            navigateToChatFragment(chatId, chatTitle);
        }
    }

    private void navigateToAdmin() {
        if (mNavController.getCurrentDestination().getId() != R.id.adminFragment) {
            mNavController.navigate(R.id.adminFragment);

        }
    }

    private void setupFirebaseAuthListener() {
        mAuthStateListener = auth -> {

            mCurrentUser = auth.getCurrentUser();
            if (mCurrentUser != null) {
                if (mCurrentUser.isEmailVerified()) {
                    navigateToChatRoomList();
                } else {
                    SnackBarUtil.showSnackbar(MainActivity.this,
                            R.string.prompt_check_verification_link
                            , android.R.string.ok, v -> {
                                //dismiss
                            });
                }
            } else {
                navigateToLogin();
            }
        };
    }

    private void initFCM() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "Fetching FCM registration token failed",
                                task.getException());
                        return;
                    }
                    // Get new FCM registration token
                    String token = task.getResult();
                    sendRegistrationTokenToServer(token);
                });
    }

    private void navigateToChatRoomList() {
        //only navigate if the current destination is login or signup fragment
        final int currentDestinationId = mNavController.getCurrentDestination().getId();
        if (currentDestinationId == R.id.loginFragment || currentDestinationId == R.id.signUpFragment) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(
                    R.id.chatRoomListFragment,
                    null,
                    new NavOptions.Builder()
                            .setPopUpTo(R.id.chatRoomListFragment, true)
                            .setPopEnterAnim(android.R.anim.slide_in_left)
                            .build());
        }
    }

    private void navigateToEditProfile() {
        if (mNavController.getCurrentDestination().getId() != R.id.editProfileFragment) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(
                    R.id.editProfileFragment);
        }
    }

    private void navigateToLogin() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.loginFragment, true).build();
        if (navController.getCurrentDestination().getId() != R.id.loginFragment) {
            navController.navigate(R.id.loginFragment, null, navOptions);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void setActionBarEnabled(Boolean enabled) {
        if (getSupportActionBar() != null) {
            if (enabled) {
                this.getSupportActionBar().show();
            } else {
                this.getSupportActionBar().hide();
            }
        }
    }

    @Override
    public void onChatRoomItemSelected(ChatRoom chatRoom) {
        navigateToChatFragment(chatRoom.getChatroom_id(), chatRoom.getChatroom_title());
    }

    private void navigateToChatFragment(String chatId, String chatTitle) {
        if (chatId != null && chatTitle != null) {
            Bundle bundle = new Bundle();
            bundle.putString(ChatFragment.CHAT_ID, chatId);
            bundle.putString(ChatFragment.TITLE, chatTitle);
            NavOptions navOptions = new NavOptions.Builder()
                    .setEnterAnim(android.R.anim.slide_in_left)
                    .setExitAnim(android.R.anim.slide_out_right)
                    .build();
            mNavController.navigate(R.id.chatFragment, bundle, navOptions);
        }
    }

    @Override
    public void onDepartmentSelected(Department department) {
        Bundle bundle = new Bundle();
        bundle.putString(DepartmentDetailFragment.DEPARTMENT_NAME, department.getDepartment_name());
        bundle.putParcelable(DepartmentDetailFragment.DEPARTMENT, department);
        NavOptions navOptions = new NavOptions.Builder()
                .setEnterAnim(android.R.anim.slide_in_left)
                .setExitAnim(android.R.anim.slide_out_right)
                .build();
        mNavController.navigate(R.id.departmentDetailFragment, bundle, navOptions);
    }

    private void sendRegistrationTokenToServer(String token) {
        Log.d(TAG, "sendRegistrationTokenToServer: sending token... " + token);
        if (mCurrentUser != null) {
            FirebaseDatabase.getInstance().getReference()
                    .child(DB_NODE_USERS)
                    .child(mCurrentUser.getUid())
                    .child(DB_FIELD_MESSAGING_TOKEN)
                    .setValue(token)
                    .addOnFailureListener(e -> {
                        Log.e(TAG, "sendRegistrationTokenToServer: ", e);
                    });
        }
    }

}