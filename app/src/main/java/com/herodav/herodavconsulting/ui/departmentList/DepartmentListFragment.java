package com.herodav.herodavconsulting.ui.departmentList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.databinding.FragmentDepartmentListBinding;
import com.herodav.herodavconsulting.databinding.ItemDepartmentBinding;
import com.herodav.herodavconsulting.model.Department;
import com.herodav.herodavconsulting.viewmodel.DepartmentsViewModel;

import java.util.List;

public class DepartmentListFragment extends Fragment {

    //todo: add departmentListener like the chatRoomListener

    private FragmentDepartmentListBinding mBinding;

    private DepartmentAdapter mAdapter;

    private DepartmentsViewModel mDepartmentsViewModel;
    private List<Department> mDepartmentList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mDepartmentsViewModel = new ViewModelProvider(this).get(DepartmentsViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentDepartmentListBinding.inflate(inflater, container, false);
        setupUi();
        getDepartments();
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding =null;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.admin_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_department:
                displayNewDepartmentDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupUi() {
        mBinding.rvDepartmentslist.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void updateUi() {
        if (mDepartmentList != null && mDepartmentList.size() > 0) {
            setupRecyclerView();
        }
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new DepartmentAdapter();
        }
        mAdapter.setDepartments(mDepartmentList);
        mBinding.rvDepartmentslist.setAdapter(mAdapter);
    }

    private void getDepartments() {
        mDepartmentsViewModel.getDepartments().observe(getViewLifecycleOwner(), departments -> {
            mDepartmentList = departments;
            updateUi();
        });
    }

    private void displayNewDepartmentDialog() {
        new NewDepartmentDialog()
                .show(getActivity().getSupportFragmentManager(), "new_department");
    }

    // ---------------------------------- RecyclerAdapter Classes ----------------------------------

    private static class DepartmentAdapter extends RecyclerView.Adapter<DepartmentHolder> {

        private List<Department> mDepartments;

        @NonNull
        @Override
        public DepartmentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ItemDepartmentBinding binding =
                    ItemDepartmentBinding.inflate(inflater, parent, false);
            return new DepartmentHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull DepartmentHolder holder, int position) {
            holder.bind(mDepartments.get(position));
        }

        @Override
        public int getItemCount() {
            return mDepartments.size();
        }

        public void setDepartments(List<Department> departments) {
            mDepartments = departments;
            notifyDataSetChanged();
        }
    }

    public static class DepartmentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public interface HolderCallback {
            void onDepartmentSelected(Department department);
        }

        private HolderCallback mCallback;
        private ItemDepartmentBinding mBinding;

        private Department mDepartment;

        public DepartmentHolder(ItemDepartmentBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            mCallback = (HolderCallback) mBinding.getRoot().getContext();
            itemView.setOnClickListener(this);
        }


        void bind(Department department) {

            mDepartment = department;
            mBinding.tvName.setText(mDepartment.getDepartment_name());
/*
            final List<String> employees = new ArrayList<>(mDepartment.getEmployeeIds());

            if (employees != null && !employees.isEmpty()) {
                lytDetail.setAdapter(new ArrayAdapter<>(
                        mContext, android.R.layout.simple_list_item_1, employees));
            }*/
        }

        @Override
        public void onClick(View v) {
            mCallback.onDepartmentSelected(mDepartment);
        }


    }

}