package com.herodav.herodavconsulting.ui.chatRoomList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.databinding.DialogNewChatroomBinding;
import com.herodav.herodavconsulting.model.ChatRoom;
import com.herodav.herodavconsulting.ui.chat.ChatFragment;
import com.herodav.herodavconsulting.utils.ToastUtil;

import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_CHATROOMS;

public class NewChatRoomDialog extends DialogFragment {
    private static final String TAG = NewChatRoomDialog.class.getSimpleName();

    private DialogNewChatroomBinding mBinding;

    private ProgressDialog mProgressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DialogNewChatroomBinding.inflate(inflater, container, false);
        setupUi();
        return mBinding.getRoot();
    }

    private void setupUi() {
        mBinding.tvConfirm.setOnClickListener(v1 -> createChatRoom());
        mBinding.tvCancel.setOnClickListener(v1 -> dismissDialog());
    }

    private void createChatRoom() {
        showProgress();
        if (isValidTitle()) {

            FirebaseUser fbUser = FirebaseAuth.getInstance().getCurrentUser();
            if (fbUser != null) {

                DatabaseReference chatRoomReference = FirebaseDatabase.getInstance().getReference()
                        .child(DB_NODE_CHATROOMS);
                //get new chatroom id
                final String chatId = chatRoomReference.push().getKey();
                final String title = mBinding.edtTitle.getText().toString().trim();

                ChatRoom chatRoom = new ChatRoom();
                chatRoom.setChatroom_title(title);
                chatRoom.setCreator_id(fbUser.getUid());
                chatRoom.setChatroom_id(chatId);

                chatRoomReference.child(chatId)
                        .setValue(chatRoom)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                hideProgress();
                                navigateToChat(chatId);
                                dismissDialog();
                            }
                        }).addOnFailureListener(e -> {
                    hideProgress();
                    ToastUtil.showCentredToast(getContext(), e.getMessage());
                    Log.e(TAG, "createChatRoom: " + e.getMessage());
                });
            }
        }
    }

    private void navigateToChat(String chatRoomId) {
        Bundle bundle = new Bundle();
        bundle.putString(ChatFragment.CHAT_ID, chatRoomId);
        NavOptions navOptions = new NavOptions.Builder()
                .setEnterAnim(android.R.anim.slide_in_left)
                .setExitAnim(android.R.anim.slide_out_right)
                .build();
        Navigation.findNavController(getActivity(), R.id.nav_host_fragment)
                .navigate(R.id.chatFragment, bundle, navOptions);
    }

    private void dismissDialog() {
        if (getDialog() != null && getDialog().isShowing()) {
            getDialog().dismiss();
        }
    }

    private boolean isValidTitle() {
        return mBinding.edtTitle.getText() != null &&
                !mBinding.edtTitle.getText().toString().trim().isEmpty();
    }

    private void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void showProgress() {
        if (mProgressDialog == null || !mProgressDialog.isShowing()) {
            mProgressDialog = ProgressDialog.show(getContext(), null, getString(R.string.please_wait));
        }
    }
}
