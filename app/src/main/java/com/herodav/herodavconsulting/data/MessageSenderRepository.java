package com.herodav.herodavconsulting.data;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.model.User;

import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_USERS;

public class MessageSenderRepository {

    private User mMessageSender;

    public MessageSenderRepository() {
    }

    public User getUser(String userId) {

        FirebaseDatabase.getInstance().getReference()
                .child(DB_NODE_USERS)
                .child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            mMessageSender = snapshot.getValue(User.class);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
        return mMessageSender;
    }
}
