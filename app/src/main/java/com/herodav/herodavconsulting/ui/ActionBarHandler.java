package com.herodav.herodavconsulting.ui;

public interface ActionBarHandler {
    void setActionBarEnabled(Boolean enabled);
}
