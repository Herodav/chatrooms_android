package com.herodav.herodavconsulting.utils;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputValidatorHelper {

    public boolean isValidEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isValidPassword(String password) {
        String PATTERN = "^[a-zA-Z]\\w{5,19}$";
//        if(allowSpecialChars){
//            //PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
//            PATTERN = "^[a-zA-Z@#$%]\\w{5,19}$";
//        }
        //else{
        //PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";

        //}

        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean isNullOrEmpty(String string) {
        return TextUtils.isEmpty(string);
    }

    public boolean isNumeric(String string) {
        return TextUtils.isDigitsOnly(string);
    }

    public boolean isCellNumber(String cellNumber) {
        final String PATTERN = "^\\d{10}$";
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(cellNumber);
        return matcher.matches();
    }

    public boolean isValidForm(String errorMessage, EditText... editTexts) {

        boolean cancel = false;
        View focusView = null;

        for (EditText editText : editTexts) {
            String content = editText.getText().toString().trim();

            if (TextUtils.isEmpty(content)) {
                editText.setError(errorMessage);
                focusView = editText;
                cancel = true;
            }
        }
        if (cancel) {
            focusView.requestFocus();
            return false;
        }
        return true;
    }
}

