package com.herodav.herodavconsulting.ui.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.databinding.ItemReceivedChatBinding;
import com.herodav.herodavconsulting.databinding.ItemSentChatBinding;
import com.herodav.herodavconsulting.model.Message;
import com.herodav.herodavconsulting.model.User;

import java.util.List;

import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_USERS;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {

    private static final int VIEW_TYPE_MESSAGE_SENT = 0;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 1;
    private List<Message> mMessageList;

    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            ItemReceivedChatBinding binding =
                    ItemReceivedChatBinding.inflate(inflater, parent, false);
            return new ReceivedMessageHolder(binding);

        } else if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            ItemSentChatBinding binding =
                    ItemSentChatBinding.inflate(inflater, parent, false);
            return new SentMessageHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, int position) {
        Message message = mMessageList.get(position);
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    public void setMessages(List<Message> messages) {
        mMessageList = messages;
    }

    //determines which Holder to display
    @Override
    public int getItemViewType(int position) {
        Message message = mMessageList.get(position);
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        //todo: set the condition here
        if (message.getUser_id().equals(userId)) {
            return VIEW_TYPE_MESSAGE_SENT;
        }
        return VIEW_TYPE_MESSAGE_RECEIVED;
    }

    static class MessageHolder extends RecyclerView.ViewHolder {

        public MessageHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    static class ReceivedMessageHolder extends MessageHolder {

        private ItemReceivedChatBinding mBinding;

        public ReceivedMessageHolder(ItemReceivedChatBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void bind(Message message) {
            if (message != null) {
                mBinding.tvText.setText(message.getText());
                FirebaseDatabase.getInstance().getReference()
                        .child(DB_NODE_USERS)
                        .child(message.getUser_id())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    final User messageSender = snapshot.getValue(User.class);
                                    mBinding.tvSender.setText(messageSender.getFullName().toLowerCase());
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });

            }
        }
    }

    static class SentMessageHolder extends MessageHolder {
        private ItemSentChatBinding mBinding;

        public SentMessageHolder(ItemSentChatBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void bind(Message message) {
            if (message != null) {
                mBinding.tvText.setText(message.getText());
            }
        }
    }
}

