package com.herodav.herodavconsulting.ui.admin;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.data.network.FCMUtil;
import com.herodav.herodavconsulting.databinding.FragmentAdminBinding;
import com.herodav.herodavconsulting.model.Department;
import com.herodav.herodavconsulting.model.fcm.Data;
import com.herodav.herodavconsulting.utils.ToastUtil;
import com.herodav.herodavconsulting.viewmodel.DepartmentsViewModel;
import com.herodav.herodavconsulting.viewmodel.EmployeeViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class AdminFragment extends Fragment {
    private static final String TAG = AdminFragment.class.getSimpleName();

    private FragmentAdminBinding mBinding;
    private DepartmentsViewModel mDepartmentsViewModel;
    private EmployeeViewModel mEmployeeViewModel;

    private HashMap<String, String> mDepartmentsMap = new HashMap<>();
    private Set<String> mSelectedDepartmentIds = new HashSet<>();
    private Set<Department> mSelectedDepartments = new HashSet<>();
    private Set<Department> mDepartments = new HashSet<>();
    private Set<String> mUserIdsSets = new HashSet<>();

    private CompositeDisposable mDisposables;

    private FirebaseUser mFbUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mFbUser = FirebaseAuth.getInstance().getCurrentUser();

        mDepartmentsViewModel = new ViewModelProvider(this).get(DepartmentsViewModel.class);
        mEmployeeViewModel = new ViewModelProvider(this).get(EmployeeViewModel.class);
        mDisposables = new CompositeDisposable();
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.btnSelectDepartment.setText(getString(R.string.select_departments));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentAdminBinding.inflate(inflater, container, false);
        setupUi();
        getDepartments();
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDisposables.clear();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
    }

    private void setupUi() {
        mBinding.cardDepartments.setOnClickListener(card -> {
            navigateToDepartmentList();
        });

        mBinding.btnSelectDepartment.setOnClickListener(btn -> {
            if (mSelectedDepartmentIds != null) {
                mSelectedDepartmentIds.clear();
            }
            displaySelectDialog();
        });

        mBinding.btnSend.setOnClickListener(btn -> {
            if (isValidForm()) {
                getSelectedDepartmentsUserIds();

                if (mUserIdsSets != null && !mUserIdsSets.isEmpty()) {
                    for (String id : mUserIdsSets) {
                        getUserAndSendMessageByUserId(id);
                    }
                }
            }
        });
    }

    private void getDepartments() {
        mDepartmentsViewModel.getDepartments().observe(getViewLifecycleOwner(),
                departments -> {
                    mDepartments.addAll(departments);

                    for (Department d : departments) {
                        mDepartmentsMap.put(d.getDepartment_name(), d.getDepartment_id());
                    }
                });
    }

    private Set<Department> getSelectedDepartments() {
        if (!mSelectedDepartmentIds.isEmpty()) {
            //get the selected departments
            for (Department department : mDepartments) {
                if (mSelectedDepartmentIds.contains(department.getDepartment_id())) {
                    mSelectedDepartments.add(department);
                }
            }
        } else {
            ToastUtil.showCentredToast(getContext(), getString(R.string.prompt_selecct_department));
        }
        return mSelectedDepartments;
    }

    private Set<String> getSelectedDepartmentsUserIds() {
        //get the users from selected departments
        if (!getSelectedDepartments().isEmpty()) {
            for (Department department : mSelectedDepartments) {
                mUserIdsSets.addAll(department.getDepartment_employeeIds());
            }
        }
        return mUserIdsSets;
    }

    private void getUserAndSendMessageByUserId(String userId) {
        mDisposables.add(mEmployeeViewModel.getUserByIdObs(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                            if (!user.getUser_id().equals(mFbUser.getUid())) {
                                FCMUtil.getInstance()
                                        .sendBroadcastMessage(
                                                getMessageData(),
                                                user.getMessaging_token());
                            }
                        }
                        , throwable -> {
                            Log.i(TAG, "getDepartmentUserIds: error getting the ids");
                        })
        );

    }

    @NotNull
    private Data getMessageData() {
        Data data = new Data();
        data.setMessage(mBinding.edtMessage.getText().toString());
        data.setTitle(mBinding.edtTitle.getText().toString());
        return data;
    }

    private void displaySelectDialog() {
        mUserIdsSets.clear();// clear the set to avoid sending data to deselected departments
        if (!mDepartmentsMap.isEmpty()) {

            final String[] departmentNames = mDepartmentsMap.keySet().toArray(new String[0]);

            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
            builder.setTitle(getString(R.string.select_department))
                    .setMultiChoiceItems(departmentNames, null, (dialog, which, isChecked) -> {
                        if (isChecked) {
                            mSelectedDepartmentIds.add(mDepartmentsMap.get(departmentNames[which]));
                        }
                    })
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                        dialog.dismiss();
                        mBinding.btnSelectDepartment.setText(R.string.reselect_departments);
                    })
                    .create()
                    .show();
        } else {
            ToastUtil.showCentredToast(getContext(), getString(R.string.error_no_departments));
        }
    }

    private boolean isValidForm() {
        boolean isValid = true;

        if (mBinding.edtTitle.getText().toString().isEmpty()) {
            mBinding.edtTitle.setError(getString(R.string.error_field_required));
            isValid = false;
        }
        if (mBinding.edtMessage.getText().toString().isEmpty()) {
            mBinding.edtMessage.setError(getString(R.string.error_field_required));
            isValid = false;
        }

        return isValid;
    }

    private void navigateToDepartmentList() {
        Navigation.findNavController(getActivity(), R.id.nav_host_fragment)
                .navigate(R.id.departmentListFragment);
    }
}