package com.herodav.herodavconsulting.ui.departmentDetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.herodav.herodavconsulting.databinding.FragmentDepartmentDetailBinding;
import com.herodav.herodavconsulting.model.Department;
import com.herodav.herodavconsulting.model.Employee;
import com.herodav.herodavconsulting.model.User;
import com.herodav.herodavconsulting.ui.employees.EmployeeAdapter;
import com.herodav.herodavconsulting.viewmodel.DepartmentsViewModel;
import com.herodav.herodavconsulting.viewmodel.EmployeeViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class DepartmentDetailFragment extends Fragment {
    private static final String TAG = DepartmentDetailFragment.class.getSimpleName();

    public static final String DEPARTMENT_ID = "department_id";
    public static final String DEPARTMENT_NAME = "department_name";
    public static final String EMPLOYEES_BRANCH_KEY = "employees_branch_key";
    public static final String ID_SET = "id_set";
    public static final String DEPARTMENT = "department";

    private FragmentDepartmentDetailBinding mBinding;

    private EmployeeAdapter mAdapter;
    private List<Employee> mEmployeeList;

    private Department mDepartment;
    private EmployeeViewModel mEmployeeViewModel;
    private DepartmentsViewModel mDepartmentsViewModel;

    private CompositeDisposable mDisposables;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mDepartment = (Department) getArguments().getParcelable(DEPARTMENT);
          /*  mDepartment.setDepartment_id(getArguments().getString(DEPARTMENT_ID));
            mDepartment.setDepartment_name(getArguments().getString(DEPARTMENT_NAME));
            mDepartment.setEmployees_branch_key(getArguments().getString(EMPLOYEES_BRANCH_KEY));*/

        }
        mEmployeeViewModel = new ViewModelProvider(this).get(EmployeeViewModel.class);
        mDepartmentsViewModel = new ViewModelProvider(this).get(DepartmentsViewModel.class);
        mDisposables = new CompositeDisposable();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentDepartmentDetailBinding.inflate(inflater, container, false);
        setupUi();
        getEmployees();
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDisposables.clear();
    }

    private void setupUi() {
        mBinding.rvEmployees.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.fabAddEmployee.setOnClickListener(fab -> displayAddEmployeeToDepartmentDialog());
    }

    private void displayAddEmployeeToDepartmentDialog() {

    }

    private void updateUi() {
        if (mEmployeeList != null && mEmployeeList.size() > 0) {
            setupRecyclerView();
        }
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new EmployeeAdapter();
        }
        mAdapter.setEmployees(mEmployeeList);
        mBinding.rvEmployees.setAdapter(mAdapter);
    }


    private void getEmployees() {
        if (mDepartment.getDepartment_employeeIds() != null
                && !mDepartment.getDepartment_employeeIds().isEmpty()) {

            mEmployeeViewModel.getUsersByIdSet(mDepartment.getDepartment_employeeIds())
                    .observe(getViewLifecycleOwner(),
                            users -> {
                                if (!users.isEmpty()) {
                                    mEmployeeList = new ArrayList<>();

                                    for (User user : users) {
                                        Employee employee = new Employee();
                                        employee.setName(user.getName());
                                        employee.setProfile_image(user.getProfile_image());
                                        employee.setUser_id(user.getUser_id());
                                        mEmployeeList.add(employee);
                                    }
                                    updateUi();
                                }
                            });
        }

    }
}