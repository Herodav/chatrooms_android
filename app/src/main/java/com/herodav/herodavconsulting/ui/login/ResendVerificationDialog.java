package com.herodav.herodavconsulting.ui.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.databinding.DialogResendVerificationBinding;
import com.herodav.herodavconsulting.utils.SnackBarUtil;
import com.herodav.herodavconsulting.utils.ToastUtil;

public class ResendVerificationDialog extends DialogFragment {
    private static final String TAG = DialogFragment.class.getSimpleName();

    private DialogResendVerificationBinding mBinding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DialogResendVerificationBinding.inflate(inflater, container, false);
        setupUi();
        return mBinding.getRoot();
    }

    private void setupUi() {
        mBinding.tvCancel.setOnClickListener(v1 -> getDialog().dismiss());
        mBinding.tvConfirm.setOnClickListener(v2 -> {
            if (isValidForm()) {
                authenticateUser();
            }
        });

    }

    private void authenticateUser() {

        final String email = mBinding.edtEmail.getText().toString();
        final String password = mBinding.edtPassword.getText().toString();
        AuthCredential credential = EmailAuthProvider.getCredential(email, password);
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        sendVerificationEmail();
                    }
                }).addOnFailureListener(e -> {
            ToastUtil.showCentredToast(getContext(), getString(R.string.auth_error));
            dismissDialog();
        });
    }

    private void sendVerificationEmail() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            dismissDialog();
                            ToastUtil.showCentredToast(getContext(), getString(R.string.verification_email_sent));
                            SnackBarUtil.showSnackbar(getActivity(), R.string.prompt_check_verification_link,
                                    android.R.string.ok, v -> {
                                        FirebaseAuth.getInstance().signOut();
                                    });
                        }
                        dismissDialog();
                    }).addOnFailureListener(e -> {
                Log.e(TAG, "sendVerificationEmail: ++++++++++++++++++++++++ " + e.getMessage());
                ToastUtil.showCentredToast(getContext(), e.getMessage());
                dismissDialog();
            });
        } else {
            ToastUtil.showCentredToast(getContext(), getString(R.string.error_user_not_logged_in));
        }

    }

    private boolean isValidForm() {
        boolean valid = true;

        if (mBinding.edtEmail.getText().toString().isEmpty()) {
            mBinding.edtEmail.setError(getString(R.string.error_field_required));
            valid = false;
        }
        if (mBinding.edtPassword.getText().toString().isEmpty()) {
            mBinding.edtPassword.setError(getString(R.string.error_field_required));
            valid = false;
        }
        return valid;
    }

    private void dismissDialog() {
        if (getDialog() != null && getDialog().isShowing()) {
            getDialog().dismiss();
        }
    }

}
