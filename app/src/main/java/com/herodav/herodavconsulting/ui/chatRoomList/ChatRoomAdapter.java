package com.herodav.herodavconsulting.ui.chatRoomList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.herodav.herodavconsulting.databinding.ItemChatroomBinding;
import com.herodav.herodavconsulting.model.ChatRoom;

import java.util.List;

public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomAdapter.ChatRoomHolder> {

    private List<ChatRoom> mChatRoomList;

    @NonNull
    @Override
    public ChatRoomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemChatroomBinding binding =
                ItemChatroomBinding.inflate(inflater, parent, false);
        return new ChatRoomHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatRoomHolder holder, int position) {
        holder.bind(mChatRoomList.get(position));
    }

    @Override
    public int getItemCount() {
        return mChatRoomList.size();
    }

    public void setChatRooms(List<ChatRoom> chatRooms) {
        this.mChatRoomList = chatRooms;
        notifyDataSetChanged();
    }


    public static class ChatRoomHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public interface HolderCallBack {
            void onChatRoomItemSelected(ChatRoom chatRoom);
        }

        private HolderCallBack mCallBack;
        private ItemChatroomBinding mBinding;
        private ChatRoom mChatRoom;

        public ChatRoomHolder(ItemChatroomBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            mCallBack = (HolderCallBack) binding.getRoot().getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mCallBack.onChatRoomItemSelected(mChatRoom);
        }

        void bind(ChatRoom chatRoom) {
            mChatRoom = chatRoom;
            mBinding.tvTitle.setText(chatRoom.getChatroom_title());
//            tvMessageNumber.setText(chatRoom.getMessage_number());
        }
    }
}
