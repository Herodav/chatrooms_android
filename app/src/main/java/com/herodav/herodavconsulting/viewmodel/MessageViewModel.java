package com.herodav.herodavconsulting.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.herodav.herodavconsulting.data.MessageRepository;
import com.herodav.herodavconsulting.model.Message;

import java.util.List;

public class MessageViewModel extends ViewModel {

    private MessageRepository mRepository;

    public MessageViewModel() {
        mRepository = new MessageRepository();
    }

    public MutableLiveData<List<Message>> getMessages(String chatRoomId) {
        return mRepository.getMessages(chatRoomId);
    }

}
