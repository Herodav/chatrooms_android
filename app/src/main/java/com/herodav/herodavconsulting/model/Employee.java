package com.herodav.herodavconsulting.model;

public class Employee extends User {

    private String departmentId;

    public Employee() {
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
}
