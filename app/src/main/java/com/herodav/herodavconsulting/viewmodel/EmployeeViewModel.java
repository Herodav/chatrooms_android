package com.herodav.herodavconsulting.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.herodav.herodavconsulting.data.EmployeeRepository;
import com.herodav.herodavconsulting.model.User;

import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class EmployeeViewModel extends ViewModel {

    private EmployeeRepository mEmployeeRepository;
    private CompositeDisposable mDisposable = new CompositeDisposable();

    public EmployeeViewModel() {
        mEmployeeRepository = new EmployeeRepository();
    }

/*    public MutableLiveData<Set<String>> getEmployees(Department department) {
        return mEmployeeRepository.getUserIds(department);
    }*/

    public MutableLiveData<Set<User>> getUsersByIdSet(Set<String> userIds) {
        return mEmployeeRepository.getUsersByIdSet(userIds);
    }

    public Observable<User> getUserByIdObs(String userId) {
        return mEmployeeRepository.getUserById(userId);
    }

    public Observable<Set<User>> getAllUsers(){
        return mEmployeeRepository.getAllUsers();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mDisposable.clear();
    }
}
