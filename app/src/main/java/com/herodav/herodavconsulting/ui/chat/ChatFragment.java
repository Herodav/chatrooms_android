package com.herodav.herodavconsulting.ui.chat;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.data.network.FCMUtil;
import com.herodav.herodavconsulting.databinding.FragmentChatBinding;
import com.herodav.herodavconsulting.model.Message;
import com.herodav.herodavconsulting.model.User;
import com.herodav.herodavconsulting.model.fcm.Data;
import com.herodav.herodavconsulting.utils.ToastUtil;
import com.herodav.herodavconsulting.viewmodel.EmployeeViewModel;
import com.herodav.herodavconsulting.viewmodel.MessageViewModel;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_CHATROOM_MESSAGES;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_CHATROOMS;

public class ChatFragment extends Fragment {
    private static final String TAG = ChatFragment.class.getSimpleName();

    public static final String CHAT_ID = "chat_id";
    public static final String TITLE = "title";

    private FragmentChatBinding mBinding;
    private MessageAdapter mAdapter;
    private List<Message> mMessageList;

    private MessageViewModel mMessageViewModel;
    private EmployeeViewModel mEmployeeViewModel;

    private String mChatId;
    private String mChatTitle;
    private ValueEventListener mMessageListener;
    private DatabaseReference mMessagesReference;

    private CompositeDisposable mDisposable = new CompositeDisposable();
    private FirebaseUser mFbUser;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFbUser = FirebaseAuth.getInstance().getCurrentUser();

        if (getArguments() != null) {
            mChatId = getArguments().getString(CHAT_ID);
            mChatTitle = getArguments().getString(TITLE);
        }
        mMessageViewModel = new ViewModelProvider(this).get(MessageViewModel.class);
        mEmployeeViewModel = new ViewModelProvider(this).get(EmployeeViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentChatBinding.inflate(inflater, container, false);
        setupUi();
        getMessages(mChatId);
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        setMessageEventListener();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMessagesReference.removeEventListener(mMessageListener);
    }

    private void setupUi() {
        mBinding.chatRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.btnSend.setOnClickListener(v1 -> {
            if (isMessageValid()) {
                sendMessage();
            }
        });
    }

    private void updateUi() {
        if (mMessageList != null && mMessageList.size() > 0) {
            setupRecyclerView();
        }
    }

    private void getMessages(String chatId) {
        mMessageViewModel.getMessages(chatId).observe(getViewLifecycleOwner(), messages -> {
            mMessageList = messages;
            updateUi();
        });
    }


    private void sendMessage() {

        if (mFbUser != null) {
            mMessagesReference = FirebaseDatabase.getInstance().getReference()
                    .child(DB_NODE_CHATROOMS)
                    .child(mChatId)
                    .child(DB_FIELD_CHATROOM_MESSAGES);

            final String messageId = mMessagesReference.push().getKey();
            final String messageText = mBinding.edtMessageText.getText().toString().trim();
            final Message message = new Message(messageText, mFbUser.getUid(), messageId);
            clearTextView();

            mMessagesReference.child(messageId).updateChildren(message.toMap())
                    .addOnSuccessListener(task ->
                    {
                        mDisposable.add(mEmployeeViewModel.getUserByIdObs(mFbUser.getUid())
                                .observeOn(Schedulers.io())
                                .subscribeOn(AndroidSchedulers.mainThread())
                                .subscribe(currentUser -> {
                                    if (currentUser.getSurname() != null){
                                        notifyUsers(currentUser.getName(), mChatId, mChatTitle, messageText);
                                    }else {
                                        notifyUsers(mFbUser.getEmail(), mChatId, mChatTitle, messageText);
                                    }
                                })

                        );
                    })
                    .addOnFailureListener(e -> {
                        ToastUtil.showCentredToast(getContext(), e.getMessage());
                        Log.e(TAG, "sendMessage: " + e.getMessage());
                    });

        }
    }

    private void notifyUsers(String userName, String chatId, String chatTitle, String messageText) {
        mDisposable.add(mEmployeeViewModel.getAllUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(users -> {
                    Data data = new Data();
                    data.setChat_id(chatId);
                    data.setTitle(chatTitle);
                    data.setMessage(messageText);
                    //todo: get username
                    if (userName != null) {
                        data.setUser_name(userName);
                    }

                    for (User user : users) {
                        if (!user.getUser_id().equals(mFbUser.getUid())) {
                            Log.d(TAG, "notifyUsers: " + userName + " " + user.getUser_id());
                            FCMUtil.getInstance().sendChatMessage(data, user.getMessaging_token());
                        }
                    }
                })

        );
    }

    private void setMessageEventListener() {

        mMessageListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //todo: make a sound
                getMessages(mChatId);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        mMessagesReference = FirebaseDatabase.getInstance().getReference()
                .child(DB_NODE_CHATROOMS)
                .child(mChatId)
                .child(DB_FIELD_CHATROOM_MESSAGES);

        mMessagesReference.addValueEventListener(mMessageListener);
    }

    private void clearTextView() {
        mBinding.edtMessageText.getText().clear();
    }

    private boolean isMessageValid() {
        return mBinding.edtMessageText.getText() != null
                && !mBinding.edtMessageText.getText().toString().trim().isEmpty();
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new MessageAdapter();
        }
        mAdapter.setMessages(mMessageList);
        mBinding.chatRecyclerview.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDisposable.clear();
    }
}