package com.herodav.herodavconsulting.utils;

import android.content.Context;
import android.widget.Toast;

public class ToastUtil {

    public static void showCentredToast(Context context, String message) {
        if (context != null) {
            final Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            toast.setGravity(0, 50, 50);
            toast.show();
        }
    }

    public static void showCentredToast(Context context, int redId) {
        if (context != null) {
            final Toast toast = Toast.makeText(context, redId, Toast.LENGTH_SHORT);
            toast.setGravity(0, 50, 50);
            toast.show();
        }
    }
}
