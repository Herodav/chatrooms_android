package com.herodav.herodavconsulting.ui.departmentList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.databinding.DialogNewDepartmentBinding;
import com.herodav.herodavconsulting.model.Department;
import com.herodav.herodavconsulting.utils.ToastUtil;

import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_EMPLOYEES_BRANCH_KEY;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_DEPARTMENTS;

//todo: ensure a bew department is added to te recyclrView when it's added
public class NewDepartmentDialog extends DialogFragment {
    private static final String TAG = NewDepartmentDialog.class.getSimpleName();

    private DialogNewDepartmentBinding mBinding;

    ProgressDialog mProgressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DialogNewDepartmentBinding.inflate(inflater, container, false);
        setupUi();
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    private void setupUi() {
        mBinding.btnCreate.setOnClickListener(btn -> {
            if (isValidForm()) {
                createDepartment();
            }
        });

        mBinding.btnCancel.setOnClickListener(btn -> {
            dismissDialog();
        });
    }

    private void createDepartment() {
        showProgress();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            DatabaseReference departmentReference = FirebaseDatabase.getInstance().getReference()
                    .child(DB_NODE_DEPARTMENTS);

            final String departmentId = departmentReference.push().getKey();
            final String departmentName = mBinding.edtName.getText().toString().trim();

            DatabaseReference employeesIdBranchReference = FirebaseDatabase.getInstance()
                    .getReference()
                    .child(DB_NODE_DEPARTMENTS)
                    .child(departmentId)
                    .child(DB_FIELD_EMPLOYEES_BRANCH_KEY);

            //prepare a branch to hold employee Ids
            final String employees_branch_key = employeesIdBranchReference.push().getKey();

            Department department = new Department();
            department.setDepartment_name(departmentName);
            department.setDepartment_id(departmentId);
            department.setEmployees_branch_key(employees_branch_key);

            departmentReference.child(departmentId)
                    .setValue(department)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            hideProgress();
                            dismissDialog();
                        }
                    })
                    .addOnFailureListener(e -> {
                        hideProgress();
                        ToastUtil.showCentredToast(getContext(), e.getMessage());
                        Log.e(TAG, "createChatRoom: " + e.getMessage());
                    });
        }

    }

    private void dismissDialog() {
        if (getDialog() != null && getDialog().isShowing()) {
            getDialog().dismiss();
        }
    }

    private boolean isValidForm() {
        return mBinding.edtName.getText() != null
                && !mBinding.edtName.getText().toString().trim().isEmpty();
    }

    private void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void showProgress() {
        if (mProgressDialog == null || !mProgressDialog.isShowing()) {
            mProgressDialog = ProgressDialog.show(getContext(), null, getString(R.string.please_wait));
        }
    }

    private void addEmployeeToDepartment(Department department, String employeeId) {

        FirebaseDatabase.getInstance().getReference()
                .child(DB_NODE_DEPARTMENTS)
                .child(department.getDepartment_id())
                .child(department.getEmployees_branch_key())
                .child(employeeId)
                .setValue(employeeId)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "addEmployeeToDepartment: employee added successfully");
                    }
                })
                .addOnFailureListener(e -> {
                    Log.i(TAG, "addEmployeeToDepartment: failed to add employee: " + e.getMessage());
                });
    }
}
