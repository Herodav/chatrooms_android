package com.herodav.herodavconsulting.ui.signup;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.herodav.herodavconsulting.R;
import com.herodav.herodavconsulting.databinding.FragmentSignUpBinding;
import com.herodav.herodavconsulting.model.User;
import com.herodav.herodavconsulting.ui.FragmentNoActionBar;
import com.herodav.herodavconsulting.utils.SnackBarUtil;
import com.herodav.herodavconsulting.utils.ToastUtil;

import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_USERS;

public class SignUpFragment extends FragmentNoActionBar {
    private static final String TAG = SignUpFragment.class.getSimpleName();

    private FragmentSignUpBinding mBinding;
    private EditText edtEmail, edtPassword, edtConfirmPassword;
    private Button btnProceed, btnToggle;
    private ProgressBar mProgressBar;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sign_up, container, false);
        setupUi(v);
        return v;
    }

    private void setupUi(View v) {
        edtEmail = v.findViewById(R.id.signup_edt_email);
        edtPassword = v.findViewById(R.id.signup_edt_password);
        edtConfirmPassword = v.findViewById(R.id.signup_edt_confirm_password);
        mProgressBar = v.findViewById(R.id.signup_progressBar);
        btnToggle = v.findViewById(R.id.signup_bnt_toggle);
        btnToggle.setOnClickListener(v1 -> navigateToLogin());
        btnProceed = v.findViewById(R.id.signup_btn_signup);
        btnProceed.setOnClickListener(v2 -> {
            if (isValidFrom()) {
                attemptSignUp();
            }
        });
    }

    private void attemptSignUp() {
        showProgress();

        final String email = edtEmail.getText().toString().trim();
        final String password = edtPassword.getText().toString();

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        hideProgress();
                        ToastUtil.showCentredToast(getContext(), R.string.sign_up_success);
                        sendVerificationEmail();

                        FirebaseUser fbUser = FirebaseAuth.getInstance().getCurrentUser();

                        User user = new User();
                        user.setName(email.substring(0, email.indexOf('@')));
                        user.setSurname("");
                        user.setPhone("");
                        user.setProfile_image("");
                        user.setSecurity_level("1");
                        user.setUser_id(fbUser.getUid());

                        FirebaseDatabase.getInstance().getReference()
                                .child(DB_NODE_USERS)
                                .child(fbUser.getUid())
                                .setValue(user).addOnFailureListener(
                                e -> ToastUtil.showCentredToast(getContext(), e.getMessage()));
                    }
                    hideProgress();
                }).addOnFailureListener(
                e -> ToastUtil.showCentredToast(getContext(), e.getMessage()));
    }

    private void sendVerificationEmail() {
        showProgress();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            hideProgress();
                            ToastUtil.showCentredToast(getContext(), getString(R.string.verification_email_sent));
                            SnackBarUtil.showSnackbar(getActivity(), R.string.prompt_check_verification_link,
                                    android.R.string.ok, v -> {
                                        FirebaseAuth.getInstance().signOut();
                                    });
                        }
                        hideProgress();
                    }).addOnFailureListener(e -> {
                Log.e(TAG, "sendVerificationEmail: ++++++++++++++++++++++++ " + e.getMessage());
                ToastUtil.showCentredToast(getContext(), e.getMessage());
            });
        } else {
            ToastUtil.showCentredToast(getContext(), getString(R.string.error_user_not_logged_in));
        }
    }

    private void hideProgress() {
        mProgressDialog.dismiss();
    }

    private void showProgress() {
        mProgressDialog = ProgressDialog.show(getContext(), null, "Please wait..");
    }

    private boolean isValidFrom() {
        //todo: implement
        boolean valid = true;

        if (edtEmail.getText().toString().isEmpty()) {
            edtEmail.setError(getString(R.string.error_field_required));
            valid = false;
        }
        if (edtPassword.getText().toString().isEmpty()) {
            edtPassword.setError(getString(R.string.error_field_required));
            valid = false;
        }
        if (edtConfirmPassword.getText().toString().isEmpty()) {
            edtConfirmPassword.setError(getString(R.string.error_field_required));
            valid = false;
        }
        if (!passwordsDoMatch()) {
            edtConfirmPassword.setError(getString(R.string.error_pwd_not_matching));
            valid = false;
        }
        if (!isValidPassword()) {
            edtPassword.setError(getString(R.string.error_short_password));
            valid = false;
        }
        /*return !edtEmail.getText().toString().isEmpty()
                && !edtPassword.getText().toString().isEmpty()
                && passwordsDoMatch();*/
        return valid;
    }

    private boolean passwordsDoMatch() {
        final String password = edtPassword.getText().toString();
        final String confirmPassword = edtConfirmPassword.getText().toString();
        return password.equals(confirmPassword);
    }

    private boolean isValidPassword() {
        final String password = edtPassword.getText().toString();
        return password.length() >= 6;
    }

    private void navigateToLogin() {

        Navigation.findNavController(getActivity(), R.id.nav_host_fragment)
                .navigate(R.id.action_signUpFragment_to_loginFragment,
                        null,
                        new NavOptions.Builder()
                                .setPopUpTo(R.id.loginFragment,
                                        true).build()
                );
    }

}