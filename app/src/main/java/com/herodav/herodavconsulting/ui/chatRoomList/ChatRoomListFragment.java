package com.herodav.herodavconsulting.ui.chatRoomList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.databinding.FragmentChatroomListBinding;
import com.herodav.herodavconsulting.model.ChatRoom;
import com.herodav.herodavconsulting.ui.FragmentActionBar;
import com.herodav.herodavconsulting.viewmodel.ChatRoomViewModel;

import java.util.List;

import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_CHATROOMS;

public class ChatRoomListFragment extends FragmentActionBar {

    private FragmentChatroomListBinding mBinding;
    private ChatRoomAdapter mAdapter;

    private ChatRoomViewModel mChatRoomViewModel;

    private List<ChatRoom> mChatRoomList;
    private ValueEventListener mChatRoomListener;
    private DatabaseReference mChatRoomReference;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChatRoomViewModel = new ViewModelProvider(this).get(ChatRoomViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentChatroomListBinding.inflate(inflater, container, false);
        setupUi();
        getChatRooms();
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        setChatRoomListener();
    }

    @Override
    public void onStop() {
        super.onStop();
        mChatRoomReference.removeEventListener(mChatRoomListener);
    }

    private void setupUi() {
        mBinding.chatroomListRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.fabNewChatroom.setOnClickListener(v1 -> openNewChatRoomDialog());
    }

    private void updateUi() {
        if (mChatRoomList != null && mChatRoomList.size() > 0) {
            setupRecyclerView();
        }
    }

    private void getChatRooms() {
        mChatRoomViewModel.getChatRooms().observe(getViewLifecycleOwner(), chatRooms -> {
            mChatRoomList = chatRooms;
            updateUi();
        });
    }

    private void setChatRoomListener() {
        mChatRoomListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                getChatRooms();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };
        mChatRoomReference = FirebaseDatabase.getInstance().getReference()
                .child(DB_NODE_CHATROOMS);
        mChatRoomReference.addValueEventListener(mChatRoomListener);
    }

    private void openNewChatRoomDialog() {
        new NewChatRoomDialog().show(getActivity().getSupportFragmentManager(), "New Chatroom");
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new ChatRoomAdapter();
        }
        mAdapter.setChatRooms(mChatRoomList);
        mBinding.chatroomListRecyclerview.setAdapter(mAdapter);
    }

}