package com.herodav.herodavconsulting.data;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.herodav.herodavconsulting.model.User;

import java.util.HashSet;
import io.reactivex.Observable;
import java.util.Set;

import io.reactivex.subjects.PublishSubject;

import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_USERS;

public class EmployeeRepository {
    private static final String TAG = EmployeeRepository.class.getSimpleName();

    private MutableLiveData<Set<User>> mUsers;

    public EmployeeRepository() {
        mUsers = new MutableLiveData<>();
    }

    public Observable<Set<User>> getAllUsers(){
        final PublishSubject<Set<User>> usersObs = PublishSubject.create();
        FirebaseDatabase.getInstance().getReference()
                .child(DB_NODE_USERS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            final Set<User> users = new HashSet<>();

                            for (DataSnapshot userSnapshot : snapshot.getChildren()){
                                final User user = userSnapshot.getValue(User.class);
                                users.add(user);
                            }
                            usersObs.onNext(users);
                            usersObs.onComplete();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
        return usersObs;
    }

    public MutableLiveData<Set<User>> getUsersByIdSet(Set<String> userIds) {
        final Set<User> users = new HashSet<>();
        for (String id : userIds) {
            FirebaseDatabase.getInstance().getReference()
                    .child(DB_NODE_USERS)
                    .child(id)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()) {
                                final User user = snapshot.getValue(User.class);
                                users.add(user);
                                mUsers.setValue(users);
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
        }
        return mUsers;
    }

    public Observable<User> getUserById(String userId) {
        final PublishSubject<User> userObs = PublishSubject.create();
        FirebaseDatabase.getInstance().getReference()
                .child(DB_NODE_USERS)
                .child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            final User user = snapshot.getValue(User.class);
                            userObs.onNext(user);
                            userObs.onComplete();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
        return userObs;
    }
}
