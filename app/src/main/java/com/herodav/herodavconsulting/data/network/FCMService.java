package com.herodav.herodavconsulting.data.network;

import com.herodav.herodavconsulting.model.fcm.FirebaseCloudMessage;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface FCMService {

    @POST("send")
    Call<ResponseBody> sendMessage(
            @HeaderMap Map<String, String> headers,
            @Body FirebaseCloudMessage message
    );
}
