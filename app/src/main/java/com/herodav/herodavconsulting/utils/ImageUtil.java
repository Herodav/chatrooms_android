package com.herodav.herodavconsulting.utils;

import android.content.Context;
import android.net.Uri;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;

public class ImageUtil {

    public static File getCompressedImage(Context context, String photoFileName) throws IOException {
        File photo = new File(context.getFilesDir(), photoFileName);
        if (photo.exists() && photo.isFile()) {
            return new Compressor(context)
                    .compressToFile(photo);
        }
        return null;
    }

    public static File getCompressedImage(Context context, Uri photoUri) throws IOException {
        File photo = new File(photoUri.getPath());
        return new Compressor(context)
                .compressToFile(photo);
    }
}
