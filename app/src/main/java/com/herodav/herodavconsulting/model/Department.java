package com.herodav.herodavconsulting.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

public class Department implements Parcelable {
    private String department_id;
    private String department_name;
    private String employees_branch_key;
    private Set<String> department_employeeIds = new HashSet<>();

    public Department() {
    }

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String id) {
        this.department_id = id;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public String getEmployees_branch_key() {
        return employees_branch_key;
    }

    public void setEmployees_branch_key(String employees_branch_key) {
        this.employees_branch_key = employees_branch_key;
    }

    public Set<String> getDepartment_employeeIds() {
        return department_employeeIds;
    }

    public void setDepartment_employeeIds(HashSet<String> department_employeeIds) {
        this.department_employeeIds = department_employeeIds;
    }

    protected Department(Parcel in) {
        department_id = in.readString();
        department_name = in.readString();
        employees_branch_key = in.readString();
        department_employeeIds = (Set) in.readValue(Set.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(department_id);
        dest.writeString(department_name);
        dest.writeString(employees_branch_key);
        dest.writeValue(department_employeeIds);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Department> CREATOR = new Parcelable.Creator<Department>() {
        @Override
        public Department createFromParcel(Parcel in) {
            return new Department(in);
        }

        @Override
        public Department[] newArray(int size) {
            return new Department[size];
        }
    };
}
