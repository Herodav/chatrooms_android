package com.herodav.herodavconsulting.ui;

import android.util.Log;

import androidx.fragment.app.Fragment;

public abstract class FragmentNoActionBar extends Fragment {
    private static final String TAG = FragmentNoActionBar.class.getSimpleName();

    @Override
    public void onStart() {
        super.onStart();
        if (getActivity() != null) {
            try {
                ((ActionBarHandler) getActivity()).setActionBarEnabled(false);
            } catch (Exception e) {
                Log.e(TAG, "onCreate: the Host activity should implement ActionBarHandler");
            }
        }
    }
}
