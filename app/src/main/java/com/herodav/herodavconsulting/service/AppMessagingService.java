package com.herodav.herodavconsulting.service;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.herodav.herodavconsulting.model.fcm.Data;
import com.herodav.herodavconsulting.notification.BroadCastNotification;
import com.herodav.herodavconsulting.notification.ChatNotification;
import com.herodav.herodavconsulting.ui.chat.ChatFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.herodav.herodavconsulting.utils.Constants.DATA_CHAT_ID;
import static com.herodav.herodavconsulting.utils.Constants.DATA_MESSAGE;
import static com.herodav.herodavconsulting.utils.Constants.DATA_TITLE;
import static com.herodav.herodavconsulting.utils.Constants.DATA_TYPE_ADMIN_BROADCAST;
import static com.herodav.herodavconsulting.utils.Constants.DATA_TYPE_CHATROOM_MESSAGE;
import static com.herodav.herodavconsulting.utils.Constants.DATA_USER_NAME;
import static com.herodav.herodavconsulting.utils.Constants.DB_FIELD_MESSAGING_TOKEN;
import static com.herodav.herodavconsulting.utils.Constants.DB_NODE_USERS;
import static com.herodav.herodavconsulting.utils.Constants.MESSAGE_DATA_TYPE;

public class AppMessagingService extends FirebaseMessagingService {
    private static final String TAG = AppMessagingService.class.getSimpleName();

    private CompositeDisposable mDisposable = new CompositeDisposable();

    @Override
    public void onNewToken(@NonNull String token) {
        sendRegistrationTokenToServer(token);
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage message) {

        String dataType = message.getData().get(MESSAGE_DATA_TYPE);

        if (dataType != null && dataType.equals(DATA_TYPE_ADMIN_BROADCAST)) {
            sendBroadCastNotification(message);
        }

        if (dataType != null && dataType.equals(DATA_TYPE_CHATROOM_MESSAGE)) {
            sendChatNotification(message);
        }
    }

    private void sendChatNotification(@NonNull RemoteMessage message) {
        Data data = new Data();
        data.setUser_name(message.getData().get(DATA_USER_NAME));
        data.setTitle(message.getData().get(DATA_TITLE));
        data.setChat_id(message.getData().get(DATA_CHAT_ID));
        data.setMessage(message.getData().get(DATA_MESSAGE));

        ChatNotification notification = new ChatNotification(this, data);
        notification.sendNotification();

    }

    private void sendBroadCastNotification(@NonNull RemoteMessage message) {
        BroadCastNotification notification = new BroadCastNotification(this);
        notification.sendNotification(message.getData().get("title"), message.getData().get("message"));
    }

    private void sendRegistrationTokenToServer(String token) {
        Log.d(TAG, "sendRegistrationTokenToServer: sending token... " + token);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            FirebaseDatabase.getInstance().getReference()
                    .child(DB_NODE_USERS)
                    .child(user.getUid())
                    .child(DB_FIELD_MESSAGING_TOKEN)
                    .setValue(token)
                    .addOnFailureListener(e -> {
                        Log.e(TAG, "sendRegistrationTokenToServer: ", e);
                    });
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mDisposable.clear();
    }
}
