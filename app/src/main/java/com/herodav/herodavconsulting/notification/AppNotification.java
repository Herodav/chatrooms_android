package com.herodav.herodavconsulting.notification;

import android.app.NotificationManager;

abstract class AppNotification {
    public abstract void sendNotification(String title, String text);

    abstract void createChannel(NotificationManager notificationManager);
}
