package com.herodav.herodavconsulting.data.network;

import android.util.Log;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

//todo: implement interceptor and handle networkResponses with networkResource
public class ServiceGenerator {
    private static final String TAG = "ServiceGenerator";

    public static <S> S createService(Class<S> serviceClass, final String baseUrl) {

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
//                .client(createOkHttpClient(token))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build()
                .create(serviceClass);
    }


    private static OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS);

//        if (token != null && !token.isEmpty()) {
            Log.d(TAG, "createOkHttpClient: adding interceptor");

            builder.addInterceptor(chain -> {
                Request originalRequest = chain.request();

                //customize the request
                Request customRequest = originalRequest.newBuilder()
//                        .header("x-access-token", token)
                        .method(originalRequest.method(), originalRequest.body())
                        .build();
                Response response = chain.proceed(customRequest);
                return response;
            });
//        }
        return builder.build();
    }
}
